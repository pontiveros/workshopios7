//
//  QMultiThreadVC.m
//  WorkshopiOS7
//
//  Created by Pedro Ontiveros on 12/25/13.
//  Copyright (c) 2013 Pedro Ontiveros. All rights reserved.
//

#import "QMultiThreadVC.h"

@interface QMultiThreadVC ()

@end

@implementation QMultiThreadVC

@synthesize localTimer;
@synthesize counter;
@synthesize progress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Multithreading";
    completed  = 0.0f;
    ticks      = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showMessage:(NSString*)message withTitle:(NSString*)title
{
    UIAlertView * alert = [[UIAlertView alloc] init];
    [alert setTitle:title];
    [alert setMessage:message];
    [alert addButtonWithTitle:@"Accept"];
    [alert show];
    [alert release];
}

- (IBAction)onTapNewThread1:(id)sender
{
    // How to create a NSThread detached.
    [NSThread detachNewThreadSelector:@selector(worker1:) toTarget:self withObject:@"Pedro Ontiveros' message"];
}

- (void)worker1:(id)param
{
    NSString *message = (NSString*)param;
    
    for (int i = 0; i < 10; i++) {
        NSLog(@"%@ tick number %d",message, (i + 1));
        completed = ((i + 1)/ 10);
        dispatch_async(dispatch_get_main_queue(), ^() {
            [self.progress setProgress:completed];
        });
        sleep(2);
    }
    
    NSLog(@"End of worker1.");
}

- (IBAction)onTapStartTimer:(id)sender
{
    // How to create a timer.
    self.localTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(runLocalTimer) userInfo:nil repeats:YES];
}

- (IBAction)onTapStopTimer:(id)sender
{
    [self.localTimer invalidate];
    self.localTimer = nil;
    ticks = 0;
}

- (void)runLocalTimer
{
#ifdef DEBUG
    NSLog(@"Timer is working in background!!!");
#endif
    
    ticks++;
    NSString * updateString = [NSString stringWithFormat:@"Counter: %d ticks!", ticks];
    [self.counter setText:updateString];
}

@end
